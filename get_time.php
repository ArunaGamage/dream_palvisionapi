<?php
$_date = explode(' ',date('d n Y H i s w'));
$_date_array = [
	'status'=>'200',
	'dd'=> $_date[0],
	'mm'=> $_date[1],
	'yy'=> $_date[2],
	'hh'=> $_date[3],
	'mn'=> $_date[4],
	'ss'=> $_date[5],
	'day'=> (string)($_date[6]+1),
];
print_r(json_encode($_date_array));