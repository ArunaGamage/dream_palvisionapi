<?php
include 'connect.php';
$sql = $_db->get_all(' SELECT `id`, `name` AS `group_description`, `desc_en` AS `desc`, `img`, `operation_hours`, `bottom_message` FROM `tb_restaurants` WHERE `enabled` = "1" ORDER BY `sort` ASC ');

if(!empty($sql)){
	foreach ($sql as $index => $row) {
		$sql[$index]['desc'] = $row['desc']."<br>".$row['operation_hours'];
	}
}
$struct = array("status" => '200',"restaurants" => $sql);
print json_encode($struct);	
?>