<?php
include 'connect.php';

$ip = $_SERVER['REMOTE_ADDR'];
// $ip ='192.168.3.124';
$movielock = 0;
$freemovie = 1;
$_filterbygroup = '';
if(!empty($_GET['i'])){
	$_filterbygroup = ' AND t.`group_movie_id` = "'.$_GET['i'].'" ';
}
$sql = $_db->get_row(' SELECT room_no, movie_lock, free_movie FROM tbh_room AS tr, hotel_db.`boxinfo` AS bi WHERE bi.IP = "'.$ip.'" AND tr.room_no = bi.roomno ');
if(!empty($sql)){
	$movielock = $sql['movie_lock'];
	$freemovie = $sql['free_movie'];
}
$sql2 = $_db->get_all(' SELECT  
							Concat(title,"") as title,
							name as ratings_id, charge, 
							director, 
							trailer,
							video_file,
							language, 
							t.id,
							t.image_file,
							playtime , 
							REPLACE(REPLACE(description ,"+", " "),"%", " ") as description,
							group_movie_id as cat_id,
							 actor, '.$freemovie.' AS free, '.$movielock.' AS movielock FROM tb_movie t LEFT JOIN tb_movie_ratings r ON t.ratings_id = r.id WHERE t.enabled=1 '.$_filterbygroup.' ORDER BY `id` DESC ');
$struct = array("status" => '200',"movies" => $sql2);
print json_encode($struct);	