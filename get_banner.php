<?php
include 'connect.php';
$name = 'GALLERY';

$_promotion_banners = $_db->get_all("SELECT id, image as img,`text` FROM tb_main_slider where enabled = 1");

$_hotel_banners = $_db->get_all("SELECT id, image as img,`text` FROM tb_hotel_banners where enabled = 1");
$mydata = array();

$_p_index = 1;
$_h_index = 0;
if(!empty($_promotion_banners)){
	foreach ($_promotion_banners as $row) {
		$mydata[$_p_index] = $row;
		$_p_index+=2;
	}
}

if(!empty($_hotel_banners)){
	foreach ($_hotel_banners as $row) {
		$mydata[$_h_index] = $row;
		$_h_index+=2;
	}
}


ksort($mydata);
$_banners = array();
foreach ($mydata as $_banner) {
	array_push($_banners,$_banner);
}		
$struct = array("status" => '200',"name" => $name,"items" => $_banners);
print json_encode($struct);	