<?php
	include 'connect.php';

	$_result = array();	
	$roomno = "";
	$ip_active = $_SERVER['REMOTE_ADDR'];
	$ip_active = '192.168.3.145'; // FOR TESTING

	if(!empty($_GET['cid'])){
		$_categoryid = $_GET['cid'];
		$_categories = $_db->get_all(' SELECT cc.`id`,cc.`title`,cc.`dresscode`,cc.`date`,cc.`sort`, ch.`name` AS `header`, ch.`img` AS `header_img` FROM `tb_conference_headers` AS ch, `tb_conference_category` AS cc WHERE cc.`title` = ch.`id` AND ch.`enabled` = "1" AND cc.`enabled` = "1" AND ch.`id` = "'.$_categoryid.'" ORDER BY `sort` ASC ');
		if(!empty($_categories)){
			foreach ($_categories as $index => $_category) {
				$_category['date'] = date('l jS F',strtotime($_category['date']));
				$_result[$index] = $_category;
				$_items = $_db->get_all(' SELECT * FROM `tb_conference_items` WHERE `category` = "'. $_category['id'] .'" AND `enabled` = "1" ORDER BY `sort` ASC ');
				$_result[$index]['items'] = array();
				foreach ($_items as $_item) {
					$_item['start_time'] = date('H:i',strtotime($_item['start_time']));
					$_item['end_time'] = date('H:i',strtotime($_item['end_time']));
					$_result[$index]['items'][] = $_item;
				}
			}
		}
	}
	echo json_encode(array("status" => '200',"conference" => $_result));
