<?php
include 'connect.php';
$a = 0;

$_bottom_message = $_db->get_row(' SELECT `id`, `bottom_message` FROM `tb_spa` WHERE `enabled` = "1" AND `id` = "1" ORDER BY `sort` ASC');
$msg = "";
if(!empty($_bottom_message)){
	$msg = $_bottom_message['bottom_message'];
}

$sql = $_db->get_all(' SELECT `id`, `category_name`, `category_name` AS root_name, `img`, `background`, `layout_type`, `spa_desc`, `enabled`, `operating_hrs` AS `time`, `sort` FROM `tb_menucategory_spa` WHERE `enabled` = "1" ORDER BY `sort` ASC ');
$mydata = array();

if(!empty($sql)){
	foreach ($sql as $index => $row) {
		$row['dummy_id'] = $index+1;
		$row['bottom_message'] = $msg;
		$mydata[] = $row;
	}
}
$struct = array("status" => '200',"message"=>$msg, "menu" => $mydata);
print json_encode($struct);