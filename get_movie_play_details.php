<?php
	include 'connect.php';

	$id = $_GET['i'];
	$cat_id = $_GET['cat_id'];
	$watched = $_GET['watched'];
	$from_free = (isset($_GET['from_free']))?$_GET['from_free']:0;
	$m_name = "";
	$play_time = "";
	// $ip_address = '192.168.3.124';
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$room_no = "";
	$endpos = 0;
	$bookmarkid = 0;
	
	$sql = $_db->get_row(' SELECT `video_file` FROM `tb_movie` WHERE `id` = "'.$id.'" ');
	if(!empty($sql)){
		$m_name = $sql['video_file'];
	}

	$sql2 = $_db->get_row(' SELECT `RoomNO` FROM hotel_db.`boxinfo` WHERE `IP` = "'.$ip_address.'" ');
	if(!empty($sql2)){
		$room_no = $sql2['RoomNO'];
	} 

	$sql3 = $_db->get_row(' SELECT `EndPos`, `id` FROM `tb_bookmark` WHERE `roomno` = "'.$room_no.'" AND `mov_id` = "'.$id.'" ORDER BY `id` DESC LIMIT 0,1 ');
	if(!empty($sql3)){
		$endpos = $sql3['EndPos'];
		$bookmarkid = $sql3['id'];
	}

	$sql5 = $_db->get_row("SELECT 
						u.UserID as userid, 
	                    u.UserName as guest_name,  
	                    u.pmsid as pmsid, 
	                    DATE_FORMAT(u.StartTime,'%Y-%m-%d %T.0')  as checkin_date, 
	                    u.UserBillPolicy as userbillpolicy 
	                FROM 
	                	hotel_db.users u INNER JOIN 
	                    hotel_db.boxinfo b ON 
	                    u.RoomNo=b.roomno 
	                WHERE 
	                	u.checkout=0 and 
	                    u.RooMNo='".$room_no."' 
	                order by 
	                	u.StartTime DESC");
						
	if(!empty($sql5)){
		$pmsid = $sql5['pmsid'];
		$userid = $sql5['userid'];
		$userbillpolicy = $sql5['userbillpolicy'];
	}

	if($endpos=="" || $endpos<=0 || $watched==0){
		$endpos=0;
	}else{
		$endpos=(int)($endpos/1000);  
	}

echo json_encode(array('id'=>$id,'cat_id'=>$cat_id,'from_free'=>$from_free,'m_name'=>$m_name,'room_no'=>$room_no,'userid'=>$userid,'endpos'=>$endpos));