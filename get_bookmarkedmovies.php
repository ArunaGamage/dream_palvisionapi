<?php
	include 'connect.php';
	$a = 0;
	$day = 0;
	
	$ip = $_SERVER['REMOTE_ADDR'];

	// $ip = '192.168.3.124'; // for testing
	
	$_filterbygroup = '';

	if(!empty($_GET['i'])){
		$_filterbygroup = ' AND `group_movie_id` = "'.$_GET['i'].'" ';
	}
	
	$_boxinfo = $_db->get_row(' SELECT `RoomNO` FROM hotel_db.`boxinfo` WHERE `IP` = "'.$ip.'" ');

	if(!empty($_boxinfo)){
		$roomno = $_boxinfo['RoomNO'];

		$freemovie = 0;
		$movielock = 0;

		$sql = $_db->get_row(' SELECT `room_no`, `movie_lock` AS ml, `free_movie` AS fm FROM `tbh_room` WHERE `room_no` = "'.$roomno.'" ');
		if(!empty($sql)){
			$movielock = $sql['ml'];
			$freemovie = $sql['fm'];
		}

		$_isfreemovie = '';
		if($freemovie==1){
			$_isfreemovie = ' AND bm.`bookmark_date` > DATE_SUB(NOW(), INTERVAL 1 DAY) ';
		}

		$pmsid = 0;

		$sql5 = $_db->get_row('
			SELECT 
				u.`UserID` AS userid, 
	            u.`UserName` AS guest_name,  
	            u.`PMSID` AS pmsid, 
	            DATE_FORMAT(u.`StartTime`,"%Y-%m-%d %T.0") AS checkin_date, 
	            u.`UserBillPolicy` AS userbillpolicy 
	        FROM 
	        	hotel_db.`users` AS u INNER JOIN 
	            hotel_db.`boxinfo` AS b ON 
	            u.`RoomNo` = b.`roomno` 
	        WHERE 
	        	u.`checkout`= "0" AND 
	            u.`RooMNo` = "'.$roomno.'" 
	        ORDER BY 
	        	u.`StartTime` DESC');
							
		if(!empty($sql5)){
			$pmsid = $sql5['pmsid'];
		}

		$sqlx = $_db->get_row('SELECT `checkout` FROM hotel_db.`users` WHERE `RooMNo` = "'.$roomno.'" ORDER BY `StartTime` DESC LIMIT 1 ');
		if(!empty($sqlx)){
			if($sqlx['checkout'] == 0){
				$movielock = 0;
			}else if($sqlx['checkout'] == 1){
				$movielock = 1;
			}	
		}
		$sql4 = $_db->get_row('
			SELECT 
				TIMESTAMPDIFF(Day, date(Now()), date(`package_end`)) AS day
	        FROM 
	        	hotel_db.`tb_movie_purchase` 
	        WHERE  
	            `roomno` = "'.$roomno.'" AND
				`pmsid` = "'.$pmsid.'" 
	        ORDER BY
	        	`package_start` DESC LIMIT 1');
		if(!empty($sql4)){
			$day = $sql4['day'];
		}						

		$freemovie = $day >= 1?1 :0;
		
		$sql2 = $_db->get_all('
			SELECT  
				Concat(t.`title`," ") AS title,
				`name` AS ratings_id, 
				`charge`, 
				`director`, 
				`trailer`,
				`video_file`,
				`language`, 
				t.`id`,
				t.`image_file`,
				t.`playtime`, 
				REPLACE(REPLACE(`description` ,"+", " "),"%", " ") AS description,
				`group_movie_id` AS cat_id,
				`actor`, 
				'.$freemovie.' AS free, 
				'.$movielock.' AS movielock,
				bm.roomno AS RooMNo
			FROM `tb_bookmark` AS bm LEFT JOIN `tb_movie` AS t ON bm.`filename` = t.`video_file` LEFT JOIN `tb_movie_ratings` AS r ON t.`ratings_id` = r.`id` 
			WHERE t.`enabled`= "1" '.$_filterbygroup.' AND bm.roomno = "'.$roomno.'" '.$_isfreemovie.' ORDER BY `id` DESC');
		if(!empty($sql2)){
			$_result = $_db->get_row(' SELECT `content` FROM `tb_instructions` WHERE `module` = "mediamanagement" AND `enabled` = "1" LIMIT 1 ');
			$_instruction = '';
			if(!empty($_result)){
				$_instruction = $_result['content'];
			}
				
			$struct = array("status" => '200',"instruction"=>$_instruction,"movies" => $sql2);
		}else{
			$struct = array("status" => '200',"instruction"=>'',"movies" => $sql2);
		}
		print json_encode($struct);	
	}
?>