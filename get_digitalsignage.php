<?php
include 'connect_demo.php';

$_signage_item = [];
$_result = array();
$_floor = !empty($_REQUEST['floor'])?$_REQUEST['floor']:'';

$_device = $_db->get_row(' SELECT * FROM  `digitalsignage_device` WHERE `enabled` = "1" AND `floor` = "'. $_floor .'" LIMIT 1 ');

$_moving_text = $_db->get_row(' SELECT `message` FROM `scrolling_message` WHERE `enabled` = "1" AND `date_start` <= NOW() AND `date_end` >= NOW() ');
$_result['moving_text']=!empty($_moving_text['message'])?$_moving_text['message']:'';

$_settings = $_db->get_all(' SELECT * FROM `digitalsignage_settings` ');
$_result['crashapp'] = 0;
$_result['logo'] = '';
if(!empty($_settings)){
	foreach ($_settings as $_setting) {
		if(!strcasecmp($_setting['name'],'crashapp'))
			$_result['crashapp'] = isset($_setting['value'])?$_setting['value']:'0';
		else if(!strcasecmp($_setting['name'],'logo'))
			$_result['logo'] = isset($_setting['value'])?'hotel/include/images/digitalsignage/logo/'.$_setting['value']:'';
		else if(!strcasecmp($_setting['name'],'hidelogo')){
			if($_setting['value']=="1")
				$_result['logo'] = '';
			break;
		}
	}
}

if(!empty($_device)){

	$_floorpan_path = 'hotel/include/images/digitalsignage/floorplan/';
	$_event_path = 'hotel/include/images/digitalsignage/item/';
	$_result['id'] = $_device['id'];
	$_result['floor'] = $_device['floor'];
	$_result['floor_image'] = $_floorpan_path.$_device['img'];
	$_result['slider_time'] = $_device['duration'];

	$_items = $_db->get_all(' SELECT * FROM  `digitalsignage_item` WHERE `enabled` = "1" AND (`device_id` = "'. $_device['id'] .'" OR `device_id` = "all") ');
	foreach ($_items as $_item) {
		
		array_push($_signage_item,array('id'=>$_item['id'],'url'=>$_event_path.$_item['img']));
	}
	$_result['slider_image_array']=$_signage_item;
	
}
print_r(json_encode($_result));
