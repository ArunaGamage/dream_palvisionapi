<?php
include 'connect.php';
$a = 0;

$roomno="";
$bg_img="";
$desc_en="";
$conf_flag=0;
$username="Guest";

if(!empty($_REQUEST['id'])){
	if(empty($_REQUEST['roomno'])){
		$ip_active = $_SERVER['REMOTE_ADDR'];

		$_guestdetails = $_db->get_row(' SELECT b.roomno, u.pmsid, u.departure, u.UserName, u.UserGrant FROM hotel_db.users u, hotel_db.boxinfo b WHERE u.RoomNo=b.roomno AND u.checkout=0 AND b.ip="'.$ip_active.'" LIMIT 1 ');
		if(!empty($_guestdetails)){
			$username = $_guestdetails['UserName'];
			$roomno= $_guestdetails['roomno'];
		}
	}else{
		$_guestdetails = $_db->get_row(' SELECT b.roomno, u.pmsid, u.departure, u.UserName, u.UserGrant FROM hotel_db.users u, hotel_db.boxinfo b WHERE u.RoomNo=b.roomno AND u.checkout=0 AND b.roomno="'.$_REQUEST['roomno'].'" LIMIT 1 ');
		if(!empty($_guestdetails)){
			$username = $_guestdetails['UserName'];
			$roomno= $_guestdetails['roomno'];
		}
	}

	$_attendees_msg = $_db->get_all("SELECT room_no,img,desc_en  FROM tb_conference_attendees_msg WHERE from_date<=NOW() and to_date>=NOW() and enabled=1");
	$mydata2 = array();

	if(!empty($_attendees_msg)){
		foreach ($_attendees_msg as $_msg) {
			$arr_room = explode(',',$_msg['room_no']);
			if (in_array($roomno, $arr_room)) {
				$desc_en=$_msg['desc_en'];
				$bg_img=$_msg['img'];
				$conf_flag=1;
			}
		}
	}

	$sql = $_db->get_all("SELECT A.desc_en, 
							   A.id,
							   A.img,
							   A.logo,
							   B.path,
							   A.background_type 
					      FROM tb_welcomepage A
					 LEFT JOIN tb_music_file B
					 		ON A.music_id = B.id
					     WHERE A.enabled=1 
					       AND A.id=".$_GET['id']." 
					  ORDER BY A.id");
					  
	$mydata = array();

	if(!empty($sql)){
		foreach ($sql as $row) {
			if($conf_flag==1){
				$row['desc_en']="<b>Dear ".$username."</b><br><br>".$desc_en;
				$row['img']=$bg_img;
				$row['background_type']=1;//1-image 2- video
			}else{
				$row['desc_en']="<b>Dear ".$username."</b><br><br>".$row['desc_en'];
			}
			$mydata[] = $row;
		}
		$struct = array("status" => '200',"welcomemsg" => $mydata);
	}else{
		$struct = array("status" => '200',"welcomemsg" => array());
	}
}else{
	$struct = array("status" => '200',"welcomemsg" => array());
}
print json_encode($struct);	
?>